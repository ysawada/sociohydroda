# HSP
1. assimilation_rome.py を走らせてみよう (python3 assimilation_rome.py)
Run assimilation_rome.py by typing "python3 assimilation_rome.py"
2. 結果の図を解釈してみる 
Please interpret figures.
3. 観測を同化しなかったらどうなるか、見てみよう(line 141-143あたりをいじればいい。詳しくは澤田まで)
What will happen if you do not assimilate observations? You can check it by modifying lines 141-143..
