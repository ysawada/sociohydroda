#!/bin/python
#
# PFSIR.py
# Sampling-Importance-Resampling Filter
# Python ver. originally used in Coupled Land and Vegetation Data Assimilation System [Sawada et al. 2015 JGR]
#
# created by Y.Sawada 20190426
#
import numpy as np

class SIRPF(object):
#
# object reprensents Sampling-Inportance-Resampling Filter and its necessary operators
# currently no localization
#
 def __init__(self, ges, analysis, omb, R, nens, nobs, inflation, kill):
  self._ges = ges     # augmented state variables (including parameters) [model dimension,nens]
  self._analysis = analysis     # augmented state variables (including parameters) [model dimension,nens]
  self._omb = omb     # observation minus simulation [nobs,nens]
  self._R = R         # observation error variance [nobs]
  self._nens = nens   # the number of ensembles [scaler]
  self._nobs = nobs   # the number of observations [scaler]
  self._inflation = inflation # tuning parameters for particle mutation by additive noise [model dimension]
  self._kill = kill   # switch to kill particles before resampling
 
 def sirfilter(self):
 #
 # main method to impliment SIR filter
 #
  #print("calculating likelihood")
  cost = self._likelihood()
  print("cost = ",cost)
  parent, Neff = self._SelectionResampleMultinomial(cost)
  print("Neff = ",Neff)
  self._AnlMutation(parent,Neff)

 def _likelihood(self):
 #
 # calculating Gaussian likelihood
 #
  cost = np.zeros((self._nens))
  for i in range(0,self._nens):
   tmp = 0.0
   for j in range(0,self._nobs):
    tmp = tmp + (self._omb[j,i]**2.0)/(2.*self._R[j])
    #print('j=', j, ', self._omb[j,i]',self._omb[j,i], ', tmp=', tmp)
   cost[i] = np.exp(-tmp)
  return cost

 def _SelectionResampleMultinomial(self,cost):
 #
 # Effective sample size
 # Y.Saw 20191003
 #
  costsum = sum(cost[:])
  weight = cost/costsum
  Neff = 0
  for i in range(0,self._nens):
   Neff = Neff + weight[i]**2.0
  Neff = 1/Neff
 #
 # Resampling particle by multinomial draw
 #
  best = max(cost[:])
  worst = min(cost[:])
  weight = np.zeros((self._nens))
  parent = np.zeros((self._nens))
  survived = np.ones((self._nens)) # 1=survived particle; 0=dead
  survivednum = 0
  costsum = 0
  j = 0
  if self._kill == 1:
 #
 # loop for killing unsuitable particles
 # 
   for i in range(0,self._nens):
    weight[i] = (cost[i]-worst)/(best-worst)
    rottery = np.random.rand()
    if rottery < weight[i]:
     survived[i] = 1
     survivednum = survivednum + 1
     costsum = costsum + cost[i]
     parent[j] = i
     j = j + 1
    else:
     survived[i] = 0
   
 #
 # multinomial draw
 #
   weight2 = np.zeros((survivednum))
   cummulativeweight = np.zeros((survivednum))
   for j in range(0,survivednum):
    weight2[j] = cost[int(parent[j])]/costsum
    cummulativeweight[j] = cummulativeweight[max(j-1,0)] + weight2[j]
   for k in range(survivednum,self._nens): # recovering all particles
    rottery = np.random.rand()
    for l in range(0,survivednum):
     if rottery < cummulativeweight[l]:
      parent[k] = parent[l]
      break

#=================no killing particle in advance=================
  elif self._kill == 0: # no killing partile in advance
   weight2 = np.zeros((self._nens))
   cummulativeweight = np.zeros((self._nens))
   costsum = sum(cost[:])
   for j in range(0, self._nens):
    weight2[j] = cost[j]/costsum
    cummulativeweight[j] = cummulativeweight[max(j-1,0)] + weight2[j]
    #print("cum", cummulativeweight[j])
   for k in range(0, self._nens): # recovering all particles
    rottery = np.random.rand()
    for l in range(0, self._nens):
     if rottery < cummulativeweight[l]:
      parent[k] = l
      break
    #print("parent", k, parent[k])
#=================no killing particle in advance=================

  return parent, Neff

 def _AnlMutation(self,parent,Neff):
 #
 # Assignning new state vectors
 #
  for i in range(0,self._nens):
   self._analysis[:,i] = self._ges[:,int(parent[i])]
 #
 # additive noize
 # following Moradkhani et al. (2005)
 #
  variance = np.zeros(len(self._analysis[:,0]))
  rottery_max = [5,1e+30,1000,20,2,5,5,10,5,5,5,50000,100000,500000,5,5]
  #rottery_min = [0.1,1e+3,500,5,0.1,5,5,10,5,5,5,50000,100000,500000,5,5]
  #rottery_min = [0.05,1e+4,75,2.0,0.1,0,0,0,0,0.0025,0,125,0,100,0,0.0025] # Y.Saw 1/50 of initial standard dev.
  #rottery_min = [0.005,1e+3,25,0.2,0.01,0,0,0,0.005,0.0025,0,125,0,100,0.00005,0.0025] # Y.Saw 1/50 of initial standard dev.
  #rottery_min = [0.005,1e+3,25,0.2,0.01,0,0,0,0.05,0.0025,0,125,0,100,0.000005,0.0025] # Y.Saw 1/50 of initial standard dev.
  #rottery_min = [0.,0.,0.,0.,0.,0,0,0,0.01,0.0025,0,125,0,100,0.000005,0.0025] # exp115.
  #rottery_min = [0.,0.,0.,0.,0.,0,0,0,0,0,0,0,0,0,0.0,0.] # exp139.
  rottery_min = [0.,0.,0.,0.,0.,0,0,0,0.01,0.0025,0,50,0,100,0.0000025,0.0025] # exp140.
  #rottery_min=0.5
  for j in range(0,len(self._ges[:,0])):
   #if j > 8:
   #inflationfactor = self._inflation[j] * (self._nens - Neff)/self._nens
   inflationfactor = self._inflation[j] * (1- (Neff/self._nens)**2)
   #else:
   # inflationfactor = self._inflation[j]
   #rottery = np.random.normal(0,np.clip((np.var(self._ges[j,:])*self._inflation[j])**0.5, rottery_min[j], rottery_max[j]),len(self._analysis[0,:]))
   #rottery = np.random.normal(0,max(self._inflation[j]*((np.var(self._ges[j,:]))**0.5), rottery_min[j]),len(self._analysis[0,:]))
   rottery = np.random.normal(0,max(inflationfactor*((np.var(self._ges[j,:]))**0.5), rottery_min[j]),len(self._analysis[0,:]))
#print 'j=', j
#  print 'variance', np.var(self._ges[j,:])
#   print 'clip', np.clip(np.var(self._ges[j,:])*self._inflation[j], 0.1,rottery_max[j])
#  print 'rottery', rottery
   #rottery = np.random.normal(0,1,len(self._analysis[0,:]))
   self._analysis[j,:] = self._analysis[j,:] + rottery[:]
   #rottery = np.random.randn(len(self._analysis[:,i]))*0.5
   #self._analysis[:,i] = self._analysis[:,i] + rottery[:]
   #print('analysis = ', self._analysis)
  return self._analysis
