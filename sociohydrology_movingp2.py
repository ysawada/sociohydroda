import math
from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt

#----parameter settings----------------------
T = 1001  #year

XiH=0.5
AlpH=0.01

EpsT=1.1
GamE=0.5 #0.5   #low cost

AlpS=0.5 #0.5

RhoE=0.02  #0.02
RmdE=5000 #5000

RmdP=12000 #12000
PhiP=5000 #10000

KapT=0.001 #0.001

MyuS=0.05     #0.05   

loc=9
scale=2.5

#sim='gumbel_input_'+str(loc)+'_scale_'+str(scale)
sim='test1'
#suptitle='KapT='+str(KapT)+', GamE='+str(GamE)+', loc='+str(loc)+', scale='+str(scale)
suptitle='GamE='+str(GamE)

#----input water level---------------------
np.random.seed(seed=14) #3)
WL = np.random.gumbel(loc,scale,T)   #gumbel distribution
W = WL-10
#W = WL # Y.Saw 20190926 
W = np.where(W<0,0,W)
#loc=9, scale=2
#W = np.random.randint(0,15,2000)   #uniform distribution

#----calculation-----------------------------

def sociohydrology(W, T, XiH, AlpH, EpsT, GamE, AlpS, RhoE, RmdE, RmdP, PhiP, KapT, MyuS, simtype):

    result = np.zeros([T,6])                 #result[t,:]=[W,F,G,D,H,M]

    #f[1,:]= [G, D, H, M]
    #f.shape=[1,4]

    def eq1(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        print("MyuS", MyuS)
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G - ((1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G+GamE*(EpsT*(W0+XiH*H0-H0))*(G**0.5))
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = (EpsT*(W0+XiH*H0-H0))- KapT*H
        dMdt = AlpS*(1-math.exp((W0+XiH*H0)/(-AlpH*D))) - MyuS*M
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)

    def eq2(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        print("MyuS", MyuS)
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G - (1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = -KapT*H
        dMdt = 1-math.exp((W0+XiH*H0)/(-AlpH*D)) - MyuS*M
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)

    def eq3(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        print("MyuS", MyuS)
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = - KapT*H
        dMdt = - MyuS*M 
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)
                

    #args=(RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS)
    H0, G0, D0, M0 = [0, 10000, 2000, 0]     #initial condition
    #F0 = [H0,G0,D0,M0]

    for t in range(T):
        W0 = W[t]
        F0 = [G0,D0,H0,M0]
        time=[t, t+1]
#
#       MyuS & KapT is changing 
#
        if (t <250):
            MyuS = 0.01
            PhiP = 5000
            #KapT = 0.001
            #GamE = 3.5
        elif(250<=t<750):
            MyuS = 0.01 + ((0.10-0.01)/500)*(t-250)
            #KapT = 0.001 + ((0-0.001)/500)*(t-250)
            #GamE = 3.5 - ((3.5-0.5)/500)*(t-250)
            PhiP = 5000 + ((40000-5000)/500)*(t-250)
        else:
            MyuS = 0.10
            #KapT = 0.0
            #GamE = 0.5
            PhiP = 40000
        args=(RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS)
        print("args", args)

        Flood = 1 - math.exp((W0+XiH*H0)/(-AlpH*D0))
        
        if W0+XiH*H0>H0:
            #F = 1-exp((W+XiH*H0)/(-AlpH*D))

            if (1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.5) and G0-(1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.5):
                #R = EpsT*(W+XiH*H0-H0)
                #S=AlpS*F
                    print('type1') 
                    print('GamE', GamE)
                    f = odeint(eq1, F0, time, args)

            else:
                print('type2') 
                print('GamE', GamE)
                f = odeint(eq2, F0, time, args)

        else:
            print('type3') 
            print('GamE', GamE)
            f = odeint(eq3, F0, time, args)
            Flood = 0
     

        print('t=',t, 'W=',W0, 'F=',Flood, 'f=',f)

        G0,D0,H0,M0 = f[1,0], f[1,1], f[1,2], f[1,3]
        result[t,:] = [ W[t], Flood, f[0,0], f[0,1], f[0,2], f[0,3] ]
        #result[t,:]=[W,F,G,D,H,M]
    return result


#result1 = sociohydrology(W, T, XiH, AlpH, EpsT, GamE, AlpS, RhoE, RmdE, RmdP, PhiP, 0.00003, MyuS, 'KapT=0.00003')

result2 = sociohydrology(W, T, XiH, AlpH, EpsT, GamE, AlpS, RhoE, RmdE, RmdP, PhiP, KapT, MyuS, 'KapT=0.0003')

#result3 = sociohydrology(W, T, XiH, AlpH, EpsT, GamE, AlpS, RhoE, RmdE, RmdP, PhiP, 0.003, MyuS, 'KapT=0.003')

result2.astype('float32').tofile('./output/verifyingtruth_movingp2_4000.bin')

#----draw------------------------------------------------------------

t1=np.arange(0,T,1)

fig = plt.figure(figsize=(10,10))
plt.rcParams['font.size']=14
#fig.rcParams['figure.figsize']=(15,15)
#plt.suptitle('KapT='+str(KapT)+', GamE='+str(GamE)+', loc='+str(loc)+', scale='+str(scale))
plt.suptitle(suptitle)

d = [result2]#[result1, result2, result3]
color = ['red']#['#5a52a3', '#eb374b', 'forestgreen']
label = ['test']#['KapT=0.00003', 'KapT=0.0003', 'KapT=0.003']

ax1 = fig.add_subplot(321)
ax1.set_xlim(0,T)
ax1.set_ylim(0,20)
ax1.set_ylabel('W(t)')
#ax1.bar(t1,result[:,0],width=0.4,color='black')
ax1.bar(t1,W,width=0.4,color='black')
plt.title('water level')

for i, (data, cl, la) in enumerate(zip(d, color, label)): 

    ax2 = fig.add_subplot(322)
    ax2.set_xlim(0,T)
    #ax2.set_ylim(0,40)#(0,20)
    ax2.set_ylabel('H(t)')
    ax2.plot(t1,data[:,4],color=cl, label=la, linewidth=0.8)
    plt.title('levee height')

    ax3 = fig.add_subplot(323)
    ax3.set_xlim(0,T)
    #ax3.set_ylim(0,4000)  #600, 2400
    ax3.set_ylabel('D(t)')
    ax3.plot(t1,data[:,3],color=cl, label=la, linewidth=0.8)
    plt.title('distance from river')

    ax4 = fig.add_subplot(324)
    ax4.set_xlim(0,T)
    #ax4.set_ylim(1e+3,1e+20)            # (1e+3,1e+12)
    ax4.set_ylabel('G(t)')
    ax4.plot(t1,data[:,2],color=cl, label=la ,linewidth=0.8)
    plt.yscale('log')
    plt.yticks([1e+3,1e+6,1e+9,1e+12])
    plt.title('settlement size')

    ax5 = fig.add_subplot(325)
    ax5.set_xlim(0,T)
    #ax5.set_ylim(0,1)
    ax5.set_ylabel('F(t)')
    ax5.scatter(t1,data[:,1],color=cl, s=5, label=la)
    plt.title('intensity of flooding')

    ax6 = fig.add_subplot(326)
    ax6.set_xlim(0,T)
    #ax6.set_ylim(0,1)   #0.6
    ax6.set_ylabel('M(t)')
    ax6.plot(t1,data[:,5],color=cl, label=la, linewidth=0.8)
    plt.title('awareness')

    plt.legend(bbox_to_anchor=(0,-0.15), loc='upper center', ncol=3)


plt.subplots_adjust(wspace=0.3, hspace=0.35)
plt.savefig('./output/testMyuSPhiPchange.png')
#plt.show()
plt.close()


print(W)
