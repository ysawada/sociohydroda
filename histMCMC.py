#!/bin/python
#
# histmcmc.py
# created by Y.Sawawa 20190610
#
#20190610 created
#20190730 updated for paper-grade figures

from pylab import *
from scipy import stats
from sklearn.neighbors import KernelDensity
import numpy as np
import matplotlib.pyplot as plt

def histplot(data,truth):
 plt.hist(data,bins=50,range=(0,1))
 yy = np.linspace(0,1000,1000)
 xx = np.ones((1000))*truth
 plt.plot(xx,yy,'r--')
 ylim(0,1000)
 xlim(0,1)
 plt.tick_params(labelsize=22)

#
# parameters
# 
truth = np.zeros((4))
truth[0] = 0.40
truth[1] = (8/3)/15
truth[2] = 0.40
truth[3] = 0.60
#
# load MCMC samples
#
data = loadtxt('./output/MCMCsamplesfrom1500_DH_timevaryingpara')
#data = loadtxt('./data_20210614/MCMCsamplesfrom1500_yz')
#
# kernel density estimation (kde)
#
x = data[:,0]
y = data[:,1]
xy = np.vstack([x,y])
print(xy)
weights = np.ones((10000))/10000.0
kde = KernelDensity(kernel='gaussian',bandwidth=0.02).fit(xy.T,sample_weight=weights)
XX, YY = np.mgrid[0:1:0.02,0:1:0.02]
positions = np.vstack([XX.ravel(),YY.ravel()])
score = kde.score_samples(positions.T)
score = score.reshape(50,50)
print(shape(score))
normalize = sum(exp(score))
plt.imshow(np.rot90(exp(score)/normalize),cmap=plt.cm.viridis)
cb = plt.colorbar(shrink=0.75)
plt.show()
#
# draw histogram
#
fig = plt.figure(figsize=(15,15))

for i in range(0,4):
 plt.subplot(2,2,i+1)
 histplot(data[:,i],truth[i])
plt.savefig('./output/hist.png')
clf()
sys.exit()
# 
# Joint distribution
#
for i in range (0,1):
 for j in range (i,2):
  heatmap, xedges, yedges = np.histogram2d(data[:,i],data[:,j],bins=50)
  plt.imshow(heatmap, extent=[0,1,1,0], interpolation='nearest')
  xlim(0,1)
  ylim(0,1) 
  cb = plt.colorbar(shrink=0.75)
  cb.ax.tick_params(labelsize=24)
  plt.tick_params(labelsize=24)
#
# calculating correlation
#
  slope, intercept, r_value, p_value, std_err = stats.linregress(data[:,i],data[:,j])
  if (p_value < 0.001):
   title = 'Slope = '+str(round(slope,3))+', R = '+str(round(r_value,3))+'(p<0.001)'
  else:
   title = 'no significant correlation'
  plt.title(title,fontsize=20)
  figname = './jointdistfrom500_'+str(i)+str(j)+'.png'
  plt.savefig(figname)
  clf()
