#!/usr/bin/env python
################################################################################
# lorenz63.py
# Ken Dixon (github: kendixon)
################################################################################
# An implementation of Lorenz, Edward Norton (1963). "Deterministic nonperiodic 
# flow".
# dx/dt = sigma * (y-x)
# dy/dt = x * ( rho - z ) - y
# dz/dt = x * y - beta * z
# The model can be integrated using the double approx. method described in L63 
# or Runge-Kutta 4 (RK4).
################################################################################

import math
from scipy,integrate import odeint
import numpy as np

class sociohydrology(object):
    
    def __init__(self, G, D, H, M, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS, dt, solver = "odeint"):
        '''
        Initialize Lorenz63 model state (x,y,z), timestep (dt), and parameters 
        (sigma, rho, and beta).
        Options for time-differencing are "RK4" and "DBAX"
        Starts at time zero.
        '''

        self._G = G 
        self._D = D 
        self._H = H
        self._M = M
        self._RhoE = RhoE
        self._RmdE = RmdE
        self._XiH = XiH
        self._AlpH = AlpH
        self._GamE = GamE
        self._RmdP = RmdP
        self._PhiP = PhiP
        self._EpsT = EpsT
        self._KapT = KapT
        self._AlpS = AlpS
        self._MyuS = MyuS
        self._dt = dt
        self._t = 0.0
        #if solver == "RK4":
        self._solver = self._odeint
        #else:
        #    self._solver = self._dbax
        self._history = {'time':[self._t],
                         'state':np.array([self._G, self._D, self._H, self._M])
                        }

    def integrate(self, n = 1):
        """
        Integrate the model state by n timesteps (default is 1 step).
        """
        for i in range(n):
            self._solver()
            self._save_state()

    def update_state(self, G, D, H, M):
        """
        Update state to posterior and save time and updated state to history so
        they can be retrieved later. 
        """
        self._G = G
        self._D = D
        self._H = H
        self._M = M
        self._save_state()

    def get_history(self):
        """
        Get the dictionary containing the list of times and corresponding 
        states (x, y, z)
        """
        return self._history

    def get_current_state(self):
        """
        Return the current time and state
        """
        return self._t, self._G, self._D, self._H, self._M

    ############################################################################
    # Solvers (Time-Differencing)
    ############################################################################
    def _save_state(self):
        """
        Save time and state to history
        """
        self._history['time'].append(self._t)
        self._history['state'] = np.vstack([self._history['state'],
                                 np.array([self._G, self._D, self._H, self._M])])

    ############################################################################
    # Solvers (Time-Differencing)
    ############################################################################
    def _odeint(self): 
        
        result = np.zeros([T,6])                 #result[t,:]=[W,F,G,D,H,M]
        #f[1,:]= [G, D, H, M], f.shape=[1,4]

        def eq1(G, D, H, M, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
            dGdt = RhoE*(1-D/RmdE)*G - ((1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G+GamE*(EpsT*(W0+XiH*H0-H0))*(G**0.05))
            dDdt = (M-D/RmdP)*PhiP/(G**0.5)
            dHdt = (EpsT*(W0+XiH*H0-H0))- KapT*H
            dMdt = AlpS*(1-math.exp((W0+XiH*H0)/(-AlpH*D))) - MyuS*M
            a = [dGdt, dDdt, dHdt, dMdt]
            return(a)

        def eq2(G, D, H, M, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
            dGdt = RhoE*(1-D/RmdE)*G - (1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G
            dDdt = (M-D/RmdP)*PhiP/(G**0.5)
            dHdt = -KapT*H
            dMdt = 1-math.exp((W0+XiH*H0)/(-AlpH*D)) - MyuS*M
            a = [dGdt, dDdt, dHdt, dMdt]
            return(a)

        def eq3(G, D, H, M, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
            dGdt = RhoE*(1-D/RmdE)*G
            dDdt = (M-D/RmdP)*PhiP/(G**0.5)
            dHdt = - KapT*H
            dMdt = - MyuS*M 
            a = [dGdt, dDdt, dHdt, dMdt]
            return(a)
            
        args=(RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS)

        for t in range(T):
            W0 = W[t]
            F0 = [G0,D0,H0,M0]
            time=[t, t+1]

            Flood = 1 - math.exp((W0+XiH*H0)/(-AlpH*D0))
    
            if W0+XiH*H0>H0:
                #F = 1-exp((W+XiH*H0)/(-AlpH*D))

                if (1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.05) and G0-(1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.05):
                    #R = EpsT*(W+XiH*H0-H0)
                    #S=AlpS*F

                    f = odeint(eq1, F0, time, args)

                else:
                    f = odeint(eq2, F0, time, args)

            else:
                f = odeint(eq3, F0, time, args)
                Flood = 0
 
            G0,D0,H0,M0 = f[1,0], f[1,1], f[1,2], f[1,3]
            #result[t,:] = [ W[t], Flood, f[0,0], f[0,1], f[0,2], f[0,3] ]


################################################################################
    def get_state(self):
        """
        Return G, D, H, M, t
        """
        return (self._G, self._D, self._H, self._M, self._t)

    def get_parameters(self):
        """
        Return system parameters
        """
        return (self._RhoE, self._RmdE, self._XiH, self._AlpH, self._GamE, self._RmdP, self._PhiP, self._EpsT, self._KapT, self._AlpS, self._MyuS)        

    def get_timestep(self):
        """
        Return dt (timestep)
        """
        return self._dt

################################################################################
################################################################################
if __name__ == '__main__':
    '''
    Test case for the Lorenz63 class that sets system parameters, time settings,
    and initial conditions to match the original Lorenz (1963) paper. Results 
    are also printed to match published results.
    '''
    # System parameters
    rho = 28; sigma = 10; beta = 8.0 / 3.0

    # Initial conditions
    x0, y0, z0 = 0.0, 1.0, 0.0

    # Time settings
    dt = 0.01
    N = 160

    # Initialize solver
    model = Lorenz63(x0, y0, z0, sigma, rho, beta, dt, solver = "DBAX")

    # Integrate
    model.integrate(n = N)

    # Plot Data
    h = model.get_history()
    for t, s in zip(h['time'][::5], h['state'][::5]):
        print("%04d % 05d % 05d % 05d" %(round(t*1e2), 10*s[0], 10*s[1], 10*s[2]))
