import math
from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt

#----parameter settings----------------------
T = 2000  #year

XiH=0.5
AlpH=0.01

EpsT=1.1
GamE=0.5   #high cost

AlpS=0.5

RhoE=0.02
RmdE=5000

RmdP=12000
PhiP=10000

KapT=0.003

MyuS=0.05

sim='gumbel_input2'
loc=9
scale=2

#----input water level---------------------
np.random.seed(seed=3)
WL = np.random.gumbel(9,2,2000)   #gumbel distribution
W = WL-11
W = np.where(W<0,0,W)

#W = np.random.randint(0,15,2000)   #uniform distribution

#----calculation-----------------------------

result = np.zeros([T,6])                 #result[t,:]=[W,F,G,D,H,M]

#f[1,:]= [G, D, H, M]
#f.shape=[1,4]

def eq1(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
    G, D, H, M = F[0],F[1],F[2],F[3]
    dGdt = RhoE*(1-D/RmdE)*G - ((1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G+GamE*(EpsT*(W0+XiH*H0-H0))*(G**0.05))
    dDdt = (M-D/RmdP)*PhiP/(G**0.5)
    dHdt = (EpsT*(W0+XiH*H0-H0))- KapT*H
    dMdt = AlpS*(1-math.exp((W0+XiH*H0)/(-AlpH*D))) - MyuS*M
    a = [dGdt, dDdt, dHdt, dMdt]
    return(a)

def eq2(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
    G, D, H, M = F[0],F[1],F[2],F[3]
    dGdt = RhoE*(1-D/RmdE)*G - (1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G
    dDdt = (M-D/RmdP)*PhiP/(G**0.5)
    dHdt = -KapT*H
    dMdt = 1-math.exp((W0+XiH*H0)/(-AlpH*D)) - MyuS*M
    a = [dGdt, dDdt, dHdt, dMdt]
    return(a)

def eq3(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
    G, D, H, M = F[0],F[1],F[2],F[3]
    dGdt = RhoE*(1-D/RmdE)*G
    dDdt = (M-D/RmdP)*PhiP/(G**0.5)
    dHdt = - KapT*H
    dMdt = - MyuS*M 
    a = [dGdt, dDdt, dHdt, dMdt]
    return(a)
            
args=(RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS)

H0, G0, D0, M0 = [0, 10000, 2000, 0]     #initial condition
#F0 = [H0,G0,D0,M0]

for t in range(T):
    W0 = W[t]
    F0 = [G0,D0,H0,M0]
    time=[t, t+1]

    Flood = 1 - math.exp((W0+XiH*H0)/(-AlpH*D0))
    
    if W0+XiH*H0>H0:
        #F = 1-exp((W+XiH*H0)/(-AlpH*D))

        if (1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.05) and G0-(1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.05):
            #R = EpsT*(W+XiH*H0-H0)
            #S=AlpS*F
                print('type1') 
                f = odeint(eq1, F0, time, args)

        else:
            print('type2') 
            f = odeint(eq2, F0, time, args)

    else:
        print('type3') 
        f = odeint(eq3, F0, time, args)
        Flood = 0
 

    print('t=',t, 'W=',W0, 'F=',Flood, 'f=',f)

    G0,D0,H0,M0 = f[1,0], f[1,1], f[1,2], f[1,3]
    result[t,:] = [ W[t], Flood, f[0,0], f[0,1], f[0,2], f[0,3] ]
    #result[t,:]=[W,F,G,D,H,M]

#----draw------------------------------------------------------------

t1=np.arange(0,T,1)

fig = plt.figure(figsize=(10,10))
#fig.rcParams["font.size"]=16
#fig.rcParams['figure.figsize']=(15,15)
plt.suptitle('KapT='+str(KapT)+', GamE='+str(GamE)+', loc='+str(loc)+', scale='+str(scale))

ax1 = fig.add_subplot(321)
ax1.set_xlim(0,T)
ax1.set_ylim(0,20)
ax1.set_ylabel('W(t)')
#ax1.bar(t1,result[:,0],width=0.4,color='black')
ax1.bar(t1,W,width=0.4,color='black')
plt.title('water level')

ax2 = fig.add_subplot(322)
ax2.set_xlim(0,T)
ax2.set_ylim(0,20)
ax2.set_ylabel('H(t)')
ax2.plot(t1,result[:,4],color='black')
plt.title('levee height')

ax3 = fig.add_subplot(323)
ax3.set_xlim(0,T)
ax3.set_ylim(600,2400)
ax3.set_ylabel('D(t)')
ax3.plot(t1,result[:,3],color='black')
plt.title('distance from river')

ax4 = fig.add_subplot(324)
ax4.set_xlim(0,T)
ax4.set_ylim(1e+3,1e+12)
ax4.set_ylabel('G(t)')
ax4.plot(t1,result[:,2],color='black')
plt.yscale('log')
plt.title('settlement size')

ax5 = fig.add_subplot(325)
ax5.set_xlim(0,T)
ax5.set_ylim(0,1)
ax5.set_ylabel('F(t)')
ax5.scatter(t1,result[:,1],color='black', s=5)
plt.title('intensity of flooding')

ax6 = fig.add_subplot(326)
ax6.set_xlim(0,T)
ax6.set_ylim(0,0.6)
ax6.set_ylabel('M(t)')
ax6.plot(t1,result[:,5],color='black')
plt.title('awareness')

plt.savefig('../output/'+str(sim)+'.png')
plt.show()
plt.close()






















