#
# sociohydrology_ensemble.py
# flood-risk model with many ensemble combinations
# needs qmcpy for sampling parameters
# created by Y.Sawada 20210615
#

import math
from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt
from qmcpy import *

#----parameter settings----------------------
nens = 1500
T = 1001  #year

XiH=0.5
AlpH=0.01

EpsT=1.1
#GamE=0.5 #0.5   #low cost

AlpS=0.5 #0.5

RhoE=0.02  #0.02
RmdE=5000 #5000

RmdP=12000 #12000
#PhiP=5000 #10000

#KapT=0.001 #0.001

#MyuS=0.05     #0.05   

#
# parameter ensemble generation
#
sobol = Sobol(dimension=4, randomize=False, graycode=True)
samples = sobol.gen_samples(n_min=2,n_max=1502)
GamE = 0.2 + samples[:,0]*4.8
PhiP = 1000 + samples[:,1]*49000
KapT = 0 + samples[:,2]*0.0015
MyuS = 0 + samples[:,3]*0.4
np.savetxt('./output/qmcsampler.txt',samples)

plt.scatter(GamE,PhiP)
plt.show()

loc=9
scale=2.5

#sim='gumbel_input_'+str(loc)+'_scale_'+str(scale)
sim='test1'
#suptitle='KapT='+str(KapT)+', GamE='+str(GamE)+', loc='+str(loc)+', scale='+str(scale)
suptitle='GamE='+str(GamE)

#----input water level---------------------
np.random.seed(seed=14) #3)
WL = np.random.gumbel(loc,scale,T)   #gumbel distribution
W = WL-10
#W = WL # Y.Saw 20190926 
W = np.where(W<0,0,W)
#loc=9, scale=2
#W = np.random.randint(0,15,2000)   #uniform distribution

#----calculation-----------------------------

def sociohydrology(W, T, XiH, AlpH, EpsT, GamE, AlpS, RhoE, RmdE, RmdP, PhiP, KapT, MyuS, simtype):

    result = np.zeros([T,6])                 #result[t,:]=[W,F,G,D,H,M]

    #f[1,:]= [G, D, H, M]
    #f.shape=[1,4]

    def eq1(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G - ((1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G+GamE*(EpsT*(W0+XiH*H0-H0))*(G**0.5))
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = (EpsT*(W0+XiH*H0-H0))- KapT*H
        dMdt = AlpS*(1-math.exp((W0+XiH*H0)/(-AlpH*D))) - MyuS*M
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)

    def eq2(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G - (1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = -KapT*H
        dMdt = 1-math.exp((W0+XiH*H0)/(-AlpH*D)) - MyuS*M
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)

    def eq3(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = - KapT*H
        dMdt = - MyuS*M 
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)
                
    args=(RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS)

    H0, G0, D0, M0 = [0, 10000, 2000, 0]     #initial condition
    #F0 = [H0,G0,D0,M0]

    for t in range(T):
        W0 = W[t]
        F0 = [G0,D0,H0,M0]
        time=[t, t+1]

        Flood = 1 - math.exp((W0+XiH*H0)/(-AlpH*D0))
        
        if W0+XiH*H0>H0:
            #F = 1-exp((W+XiH*H0)/(-AlpH*D))

            if (1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.5) and G0-(1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.5):
                #R = EpsT*(W+XiH*H0-H0)
                #S=AlpS*F
    #                print('type1') 
                    f = odeint(eq1, F0, time, args)

            else:
     #           print('type2') 
                f = odeint(eq2, F0, time, args)

        else:
      #      print('type3') 
            f = odeint(eq3, F0, time, args)
            Flood = 0
     

       # print('t=',t, 'W=',W0, 'F=',Flood, 'f=',f)

        G0,D0,H0,M0 = f[1,0], f[1,1], f[1,2], f[1,3]
        result[t,:] = [ W[t], Flood, f[0,0], f[0,1], f[0,2], f[0,3] ]
        #result[t,:]=[W,F,G,D,H,M]
    return result


#result1 = sociohydrology(W, T, XiH, AlpH, EpsT, GamE, AlpS, RhoE, RmdE, RmdP, PhiP, 0.00003, MyuS, 'KapT=0.00003')

output = np.zeros((nens,8))
for i in range(0,nens):
    #print('trial ',i, samples[i,:])
    result2 = sociohydrology(W, T, XiH, AlpH, EpsT, GamE[i], AlpS, RhoE, RmdE, RmdP, PhiP[i], KapT[i], MyuS[i], 'KapT=0.0003')

#result3 = sociohydrology(W, T, XiH, AlpH, EpsT, GamE, AlpS, RhoE, RmdE, RmdP, PhiP, 0.003, MyuS, 'KapT=0.003')
    data = result2
    print(data)
    #result2.astype('float32').tofile('./output/ens'+str(i)+'.bin')
    output[i,0] = i
    output[i,1] = GamE[i]
    output[i,2] = PhiP[i]
    output[i,3] = KapT[i]
    output[i,4] = MyuS[i]
    output[i,5] = np.mean(data[0:T:10,3]*data[0:T:10,3]) # D
    output[i,6] = np.mean(data[0:T:10,4]*data[0:T:10,4]) # H
    output[i,7] = np.mean(data[0:T:10,3]*data[0:T:10,4]) # D * H
    print('entry = ', i, output[i,:])
np.savetxt('./output/ensemblescores_4d', output)

