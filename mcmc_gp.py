#!/bin/python
#
# MCMC_GP_lhsvscost.py
# Purpose: To generate Gaussian Process statistical model which mimicks the MAE score in Lorenz63 multi-parameter ensembles
# MCMC sampling scheme
# created by Y.Sawawa 20210603
#

# Input: parameter data & cost function data
#

from pylab import *
import numpy as np
import matplotlib.pyplot as plt
from sklearn.gaussian_process import kernels as sk_kern
from sklearn.gaussian_process import GaussianProcessRegressor
from numpy.random import *

def plot_result(x_test, mean, std):
    plt.plot(x_test, mean, color="C0", label="predict mean")
    plt.fill_between(x_test, mean + std, mean - std, color="C0", alpha=.3,label= "1 sigma confidence")
#    plt.plot(x_train, y_train, "o",label= "training data")

def costfunc(mean0, mean1, std0, std1, obserr, ans0, ans1):
    cost = 1/2*((mean0-ans0)**2/(std0**2+obserr**2)+(mean1-ans1)**2/(std1**2+obserr**2)) + 1/2*log(std0**2+std1**2+obserr**2)
    return cost

def costfunc3d(mean0, mean1, mean2, std0, std1, std2, obserr, ans0, ans1, ans2):
    cost = 1/2*((mean0-ans0)**2/(std0**2+obserr**2)+(mean1-ans1)**2/(std1**2+obserr**2) + (mean2-ans2)**2/(std2**2+obserr**2)) + 1/2*log(std0**2+std1**2+std2**2+obserr**2)
    return cost

nens = 1500
subsample = 500
obserr = 0.0

# read observation
truth = np.fromfile('./output/verifyingtruth_movingp2_4000.bin','float32').reshape(1001,6) #W,F,G,D,H,M
obs = np.zeros((101,6))
j = 0
for i in range(0,1001,10):
    obs[j,:] = truth[i,:] + np.random.randn(6)
    j = j + 1

print('j = ',j)
print(obs)

# Target statistics
DDstatemean = np.mean(obs[:,3]*obs[:,3])
HHstatemean = np.mean(obs[:,4]*obs[:,4])
#xystatemean = np.mean(obs[:,0]*obs[:,1])
#zzstatemean = np.mean(obs[:,2]*obs[:,2])

#print(xstatemean,ystatemean)
# z may not be observed

# evaluating cost function
data = loadtxt('./output/ensemblescores_4d')
y_train_raw = np.zeros((nens,3))
for i in range(0,nens):
    #y_train_raw[i] = (data[i,4]-xstatemean)**2 + (data[i,5]-ystatemean)**2 + (data[i,6]-zstatemean)**2
    #y_train_raw[i] = 1/2*((data[i,7]-xxstatemean)**2 + (data[i,8]-yystatemean)**2 + (data[i,9]-zzstatemean)**2 + (data[i,10]-xystatemean)**2)
    #y_train_raw[i] = 1/2*((data[i,7]-xxstatemean)**2 + (data[i,8]-yystatemean)**2 + (data[i,10]-xystatemean)**2)
    #y_train_raw[i] = 1/2*((data[i,8]-yystatemean)**2 + (data[i,9]-zzstatemean)**2)
    y_train_raw[i,0] = data[i,5]
    y_train_raw[i,1] = data[i,6]
    #y_train_raw[i,2] = data[i,10]
    #print(i, y_train_raw[i])
y_train = y_train_raw
#y_train = (y_train_raw - min(y_train_raw))/(max(y_train_raw) - min(y_train_raw))
x_train_raw = data[:,1:5]
x_train = np.zeros((nens,4))
x_train[:,0] = (x_train_raw[:,0] - min(x_train_raw[:,0]))/(max(x_train_raw[:,0]) - min(x_train_raw[:,0]))
x_train[:,1] = (x_train_raw[:,1] - min(x_train_raw[:,1]))/(max(x_train_raw[:,1]) - min(x_train_raw[:,1]))
x_train[:,2] = (x_train_raw[:,2] - min(x_train_raw[:,2]))/(max(x_train_raw[:,2]) - min(x_train_raw[:,2]))
x_train[:,3] = (x_train_raw[:,3] - min(x_train_raw[:,3]))/(max(x_train_raw[:,3]) - min(x_train_raw[:,3]))
#x_train[:,2] = (x_train_raw[:,2] - min(x_train_raw[:,2]))/(max(x_train_raw[:,2]) - min(x_train_raw[:,2]))

#plt.scatter(x_train_raw[:,0],y_train_raw[:,0])
#plt.show()

print(shape(y_train))
# defining kernels of GP
#kernel = sk_kern.RBF() + sk_kern.RBF()*sk_kern.ExpSineSquared() + sk_kern.RationalQuadratic()  +  sk_kern.WhiteKernel() + sk_kern.DotProduct()**2
kernel = sk_kern.Matern()
clf0 = GaussianProcessRegressor(
    kernel=kernel,
    #kernel=None,
    alpha=1e-10, #1e-10, 
    optimizer="fmin_l_bfgs_b", 
    n_restarts_optimizer=80,
    normalize_y=True)
#clf0.fit(x_train,y_train[:,0])
clf1 = GaussianProcessRegressor(
    kernel=kernel,
    #kernel=None,
    alpha=1e-10, #1e-10, 
    optimizer="fmin_l_bfgs_b",
    n_restarts_optimizer=80,
    normalize_y=True)
#clf1.fit(x_train,y_train[:,1])
#clf2 = GaussianProcessRegressor(
#    kernel=kernel,
#    #kernel=None,
#    alpha=1e-10, #1e-10,
#    optimizer="fmin_l_bfgs_b",
#    n_restarts_optimizer=80,
#    normalize_y=True)

#clf0.fit(x_train[0:subsample,:],y_train[0:subsample,0])
#clf1.fit(x_train[0:subsample,:],y_train[0:subsample,1])
#pred_mean, pred_std = clf1.predict(x_train[subsample:nens,:],return_std=True)
#plt.scatter(pred_mean,y_train[subsample:nens,1])
#plt.show()
clf0.fit(x_train[0:nens,:],y_train[0:nens,0])
clf1.fit(x_train[0:nens,:],y_train[0:nens,1])
#clf2.fit(x_train[0:nens,:],y_train[0:nens,2])


#sys.exit()
#xx = np.zeros((100,2))
#xx[:,0] = 10/15
#xx[:,0] = 0.4
#xx[:,1] = np.linspace(0,1,100)
#print('xx = ', xx)
#pred_mean0, pred_std0 = clf0.predict(xx[:,:],return_std=True)
#pred_mean1, pred_std1 = clf1.predict(xx[:,:],return_std=True)
#pred_mean2, pred_std2 = clf2.predict(xx[:,:],return_std=True)
#print(shape(pred_mean), pred_mean)

#plt.scatter(xx[:,1], costfunc(pred_mean0[:], pred_mean1[:], pred_std0[:], pred_std1[:], obserr, DDstatemean, HHstatemean))
#plt.scatter(xx[:,1], costfunc3d(pred_mean0[:], pred_mean1[:], pred_mean2[:], pred_std0[:], pred_std1[:], pred_std2[:], obserr, xxstatemean, yystatemean, xystatemean))
#plt.show()


#
# MCMC samplier
#
iteration = 10000 # number of samples
ndim = 4 # dimension
x = np.zeros((iteration,ndim))
x[0,:] = 0.5 # initial condition
inputx = np.zeros((2,ndim))
inputx[0,:] = x[0,:]
inputx[1,:] = x[0,:]
pred_mean0,pred_std0 = clf0.predict(inputx,return_std=True)
pred_mean1,pred_std1 = clf1.predict(inputx,return_std=True)
#pred_mean2,pred_std2 = clf2.predict(inputx,return_std=True)
#pred_mean = pred_mean *(max(y_train_raw)-min(y_train_raw)) + min(y_train_raw)
#f = normal(pred_mean[0],pred_std[0]i)
#if pred_mean[0] < 0:
#    print("GP Error?")
#    sys.exit()
#f = min([exp(-pred_mean[0]/obserr),1])
starttime = int(np.random.uniform(0,101-10))
DDstatemean = np.mean(obs[starttime:starttime+10,3]*obs[starttime:starttime+10,3])
HHstatemean = np.mean(obs[starttime:starttime+10,4]*obs[starttime:starttime+10,4])
f = costfunc(pred_mean0[0], pred_mean1[0], pred_std0[0], pred_std1[0], obserr, DDstatemean, HHstatemean)
#f = costfunc3d(pred_mean0[0], pred_mean1[0], pred_mean2[0], pred_std0[0], pred_std1[0], pred_std2[0], obserr, xxstatemean, yystatemean, xystatemean)
print('first f = ',f)
print('starting MCMC sample')
for t in range(1,iteration):
 print('sampling no. ',t)
 proposedx = - np.ones((ndim))
 for i in range(0,ndim):
  while proposedx[i] < 0 or proposedx[i] > 1:
   proposedx[i] = normal(x[t-1,i],0.05)
 #
 # evaluate proposalx
 #
 inputx[0,:] = proposedx
 inputx[1,:] = proposedx
 #pred_mean,pred_std = clf.predict(inputx,return_std=True)
 pred_mean0,pred_std0 = clf0.predict(inputx,return_std=True)
 pred_mean1,pred_std1 = clf1.predict(inputx,return_std=True)
# pred_mean2,pred_std2 = clf2.predict(inputx,return_std=True)
 #proposedf = normal(pred_mean[0],pred_std[0])
# pred_mean = pred_mean *(max(y_train_raw)-min(y_train_raw)) + min(y_train_raw)
# proposedf = min([exp(-pred_mean[0]/obserr),1])
 #proposedf = pred_mean[0]/(pred_std[0]+obserr**2) + 1/2*log(pred_std[0]+obserr**2)
 starttime = int(np.random.uniform(0,101-30))
 DDstatemean = np.mean(obs[starttime:starttime+30,3]*obs[starttime:starttime+30,3])
 HHstatemean = np.mean(obs[starttime:starttime+30,4]*obs[starttime:starttime+30,4])
 proposedf = costfunc(pred_mean0[0], pred_mean1[0], pred_std0[0], pred_std1[0], obserr, DDstatemean, HHstatemean)
 #proposedf = costfunc3d(pred_mean0[0], pred_mean1[0], pred_mean2[0], pred_std0[0], pred_std1[0], pred_std2[0], obserr, xxstatemean, yystatemean, xystatemean)
# if pred_mean[0] < 0:
#    print("GP Error? preposedf is set to 0")
#    proposedf = 0.0
 #alpha = proposedf/f
 alpha = min(1,exp(-(proposedf-f)))
 print('proposedf and alpha = ',f, proposedf,alpha,pred_std0[0], pred_std1[0])
 #
 # sampling/rejecting
 #
 if alpha > 1:
  x[t,:] = proposedx
  f = proposedf
 else:
  rottery = rand()
  if rottery < alpha:
   x[t,:] = proposedx
   f = proposedf
  else:
   x[t,:] = x[t-1,:]
#
# output
#
np.savetxt('./output/MCMCsamplesfrom1500_DH_timevaryingpara',x)





