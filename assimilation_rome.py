#
# This version simulates Tiber river at Rome, Italy
# using the data from Ciullo et al. 2017 HSJ
# 20200325 created
#

import math
from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt
from SIRPF import *
from pylab import *
from numba import jit
from sklearn.metrics import mean_squared_error

para_switch = 1
movpara_switch = 0
spinupyr = 0 # Y.Saw 20191011
param = 'RmdP'
#suptitle = 'obs_'+str(param)


#----data assimilation parameters-------------------
ndim = 16                 # state:5 + parameters:11
nens = 5000
window = 1               # da window time steps
ncycle = 221            # total integration time = window * ncycle
nobs = 5                  # number of observed state variables   ########


#----input water level---------------------
data = np.loadtxt('./rome_input/waterlev')
W = data[:,1]
Wens = np.zeros((window*ncycle,nens))
rainerror = 0.1
for i in range(0,nens):
 Wens[:,i] = W * np.random.lognormal(0, rainerror, (window*ncycle)) 



R = np.zeros((nobs))
#R[:] = 1000000  #0.5**2
#R = [0.1**2,(1e+4)**2,100**2,2.0**2,0.2**2]    #D:(1e+10)**2
#R = [0.05**2,(1e+3)**2,250**2,2.0**2,0.1**2]    #D:(1e+10)**2
#R = [0.05**2,(1e+3)**2,250**2,2.0**2,0.1**2]    #D:(1e+10)**2
#R = [0.025**2,(0.5e+3)**2,50**2,1.0**2,0.1**2]    #D:(1e+10)**2
#R = [0.025**2,(0.5e+3)**2,125**2,0.5**2,0.05**2]    #D:(1e+10)**2
#R = [1.0**2,(2e+4)**2,150**2,4.0**2,0.2**2]    #D:(1e+10)**2
#R = [0.1**2, (0.5e+3)**2, 250**2, 2.0**2, 0.1**2]
#Re = [0.1,0.1,0.1,0.1,0.1] # Y.Saw 20191011
Re = [0.1,0.25,0.1,0.1,0.1] # Y.Saw 20200325

s2 = np.zeros((ndim))
s2[0] = 0.00 #0.01 #state
s2[1] = 0.00 #0.01  #0.00005!!!
s2[2] = 0.00 #0.0005
s2[3] = 0.00 #0.005 #0.0005
s2[4] = 0.00 #0.05
s2[5:] = 0.05 #parameter
#s2[8] = 0.5
#s2[10] = 0.5

#----model parameter settings----------------------
#T = 2000  #year

XiH1=0.0; AlpH1=0.01   #XiH1 = 0 in Rome real-world experiment
EpsT1=1.1; GamE1=0.5   #high cost
AlpS1=0.5
RhoE1=0.02; RmdE1=5000
RmdP1=12000; PhiP1=10000
KapT1=0.001
MyuS1=0.05


#----initial condition------------------------------------#######
#---reading parameter file
ges = np.zeros((ndim,nens))      # states  F,G,D,H,M
j = 0 # counter
for line in open('./rome_input/parameters.txt','r'):
 if line[0] != "#":
  ges[j,0] = line
  j = j + 1
# add perturbation
for i in range(0,5): # states
 #if i == 1:
  ges[i,1:nens] = np.random.normal(ges[i,0],ges[i,0]*Re[i],(1,nens-1))
 #else:
 # ges[i,1:nens] = np.random.normal(ges[i,0],R[i]**(1/2),(1,nens-1))
#R[1] = log10(1e+3)**2 # Y.Saw
ges[1,:] = np.random.uniform(1500,50000,(1,nens)) #G
ges[2,:] = np.random.uniform(1000,5000,(1,nens)) # D

for i in range(5,ndim): # parameters
 ges[i,1:nens] = ges[i,0] # no perturbation

if para_switch == 1: # parameter optimization
 ges[8,:] = np.random.uniform(0.2,5,(1,nens)) # GamE
 #ges[9,:] = np.random.uniform(0.0,0.75,(1,nens))
 #ges[9,:] = np.random.normal(0.25,0.025*5,(1,nens)) #AlpS
 #ges[11,:] = np.random.uniform(2500,12500,(1,nens))
 #ges[11,:] = np.random.normal(2500,1250*5,(1,nens)) #RmdE
 #ges[12,:] = np.random.uniform(1000,15000,(1,nens))
 #ges[12,:] = np.random.normal(9000,1600,(1,nens))
 ges[13,:] = np.random.uniform(1000,50000,(1,nens))
 #ges[13,:] = np.random.normal(15000,1000*5,(1,nens)) #PhiP
 ges[14,:] = np.random.uniform(0.,0.0015,(1,nens))
 #ges[14,:] = np.random.normal(0.002,0.0005,(1,nens))  #KapT
 ges[15,:] = np.random.uniform(0.,0.4,(1,nens))
 #ges[15,:] = np.random.normal(0.20,0.025*5,(1,nens)) # MyuS


ges[0,:] = np.clip(ges[0,:],0,1)  #F
ges[1,:] = np.clip(ges[1,:],10,1e+50)   #G
ges[2,:] = np.clip(ges[2,:],1,10000)   #D
ges[3,:] = np.clip(ges[3,:],0,50)    #H
ges[4,:] = np.clip(ges[4,:],0,3)    #M

ges[5,:] = np.clip(ges[5,:],0.001,50)
ges[6,:] = np.clip(ges[6,:],0.0001,10)
ges[7,:] = np.clip(ges[7,:],0.01,100)
ges[8,:] = np.clip(ges[8,:],0.001,50)
ges[9,:] = np.clip(ges[9,:],0.001,0.999) #AlpS
ges[10,:] = np.clip(ges[10,:], 0.0001,20)
ges[11,:] = np.clip(ges[11,:],100,200000)
ges[12,:] = np.clip(ges[12,:],1,150000)
ges[13,:] = np.clip(ges[13,:],1000,150000)
ges[14,:] = np.clip(ges[14,:],0.,0.01)
ges[15,:] = np.clip(ges[15,:],0.,0.5)

for i in range(0,nens):
 print ('init_ges[F,G,D,H,M]', ges[0:5,i])
print('init_para', ges[5:ndim,0])
#suptitle = 'obs_'+str(param)+'  true=0.5,init=10,var=20'

#----observations---------------------------------------

obs = np.zeros((window*ncycle,5))
obs[:,:] = -999.0
data = np.loadtxt('./rome_input/settlementsize') # G
print(len(data[:,1]),len(obs[:,1]))
obs[:,1] = data[:,1] #G
data = np.loadtxt('./rome_input/leveeheight') # H
obs[:,3] = data[:,1] #H


#----model setting-----------------------------

result = np.zeros([window+1,5])                 #result[t,:]=[F,G,D,H,M]
#f[1,:]= [G, D, H, M]
#f.shape=[1,4]

def sociohydrology(window, startt, F, G, D, H, M, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS, We):

    def eq1(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G - ((1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G+GamE*(EpsT*(W0+XiH*H0-H0))*(G**0.5))
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = (EpsT*(W0+XiH*H0-H0))- KapT*H
        dMdt = AlpS*(1-math.exp((W0+XiH*H0)/(-AlpH*D))) - MyuS*M
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)

    def eq2(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G - (1-math.exp((W0+XiH*H0)/(-AlpH*D)))*G
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = -KapT*H
        dMdt = 1-math.exp((W0+XiH*H0)/(-AlpH*D)) - MyuS*M
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)

    def eq3(F, time, RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS):
        G, D, H, M = F[0],F[1],F[2],F[3]
        dGdt = RhoE*(1-D/RmdE)*G
        dDdt = (M-D/RmdP)*PhiP/(G**0.5)
        dHdt = - KapT*H
        dMdt = - MyuS*M 
        a = [dGdt, dDdt, dHdt, dMdt]
        return(a)
            
    args=(RhoE, RmdE, XiH, AlpH, GamE, RmdP, PhiP, EpsT, KapT, AlpS, MyuS)

    #H0, G0, D0, M0 = [0, 10000, 2000, 0]     #initial condition
    H0, G0, D0, M0 = [H, G, D, M]
    result[0,:] = [F,G,D,H,M]

    for T in range(window):
        W0 = We[startt+T]
        F0 = [G0,D0,H0,M0]
        time=[T, T+1]
        #print(G0, W0, H0, D0, RmdP, PhiP)
        Flood = 1 - math.exp((W0+XiH*H0)/(-AlpH*D0))

        if W0+XiH*H0>H0:
            #F = 1-exp((W+XiH*H0)/(-AlpH*D))

            if (1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.5) and G0-(1-math.exp((W0+XiH*H0)/(-AlpH*D0)))*G0>GamE*(EpsT*(W0+XiH*H0-H0))*(G0**0.5):
                #R = EpsT*(W+XiH*H0-H0)
                #S=AlpS*F 
                f = odeint(eq1, F0, time, args)

            else: 
                f = odeint(eq2, F0, time, args)

        else: 
            f = odeint(eq3, F0, time, args)
            Flood = 0

        G0,D0,H0,M0 = f[1,0], f[1,1], f[1,2], f[1,3]
        #result[t,:] = [ W[t], Flood, f[0,0], f[0,1], f[0,2], f[0,3] ]
        #result[t,:]=[W,F,G,D,H,M]
        result[T+1,:]=[Flood,G0,D0,H0,M0]
    return(result)


#----starting da cycles------------------------------------

ensemble = np.zeros((window*ncycle,ndim,nens))
ensemblemean = np.zeros((window*ncycle,ndim))

#@jit
def PFcycle(ges, ncycle):
    for t in range(0,ncycle):
        startt = window*t
        endt = window*(t+1)

        #----STEP 1: model integration (ensemble forecast step)-----

        for i in range(0,nens):

            if para_switch == 0:
             model = sociohydrology(window, startt, ges[0,i], ges[1,i], ges[2,i], ges[3,i], ges[4,i], \
              RhoE1, RmdE1, XiH1, AlpH1, GamE1, RmdP1, PhiP1, EpsT1, KapT1, AlpS1, MyuS1, Wens[:,i])
            if para_switch == 1:
             model = sociohydrology(window, startt, ges[0,i], ges[1,i], ges[2,i], ges[3,i], ges[4,i], \
              #RhoE1, ges[11,i], XiH1, AlpH1, GamE1, ges[12,i], ges[13,i], EpsT1, KapT1, ges[9,i], ges[15,i],Wens[:,i])
              #RhoE1, ges[11,i], XiH1, AlpH1, GamE1, RmdP1, ges[13,i], EpsT1, KapT1, ges[9,i], ges[15,i], Wens[:,i])
              RhoE1, RmdE1, XiH1, AlpH1, ges[8,i], RmdP1, ges[13,i], EpsT1, ges[14,i], AlpS1, ges[15,i], Wens[:,i])
           
            ensemble[startt:endt,0:5,i] = model[1:window+1,:]
            ensemble[startt:endt,5:16,i] = ges[5:16,i]
#ensemble[startt:endt,1,i] = ensemble[startt:endt,1,i]#/(1e+5)
            #print('ges2', ges[2,i])
        #print('t=', t,',model=', model[0,:])
      
        ensemblemean[startt:endt,:] = np.mean(ensemble[startt:endt,:,:],axis=2)
        ges[:,:] = ensemble[endt-1,:,:] # new ges

        ges[0,:] = np.clip(ges[0,:],0,1)  #F
        ges[1,:] = np.clip(ges[1,:],10,1e+50)   #G
        ges[2,:] = np.clip(ges[2,:],1,10000)   #D
        ges[3,:] = np.clip(ges[3,:],0,50)    #H
        ges[4,:] = np.clip(ges[4,:],0,3)    #M
    
        ges[5,:] = np.clip(ges[5,:],0.001,50)
        ges[6,:] = np.clip(ges[6,:],0.0001,10)
        ges[7,:] = np.clip(ges[7,:],0.01,100)
        ges[8,:] = np.clip(ges[8,:],0.001,50)

        ges[9,:] = np.clip(ges[9,:],0.001,0.999)
        ges[10,:] = np.clip(ges[10,:], 0.0001,20)
        ges[11,:] = np.clip(ges[11,:],100,200000)
        ges[12,:] = np.clip(ges[12,:],1,150000)
        ges[13,:] = np.clip(ges[13,:],1000,150000)
        ges[14,:] = np.clip(ges[14,:],0.,0.01)
        ges[15,:] = np.clip(ges[15,:],0.,0.5)



        #----STEP 2: comparing observations----------------------
        # Here we have no complicated observation operator
        #
        omb = np.zeros((nobs,nens))
        counter = 0
        for i in range(0,nens):
         for j in range(0,nobs):
            #print('obs', obs[endt-1,0])
            #print('ges', ges[2,i])
            if obs[endt-1,j] > -9.0:
             omb[j,i] = obs[endt-1,j] - ges[j,i]
             counter = counter + 1
        # R of G is adaptively modified
        for i in range(0,nobs):
            #R[i] = max(0.001,(obs[endt-1,i]*Re[i])**2.0) # Y.Saw 20191004
            R[i] = max(0.1,(obs[endt-1,i]*Re[i])**2.0) # Y.Saw 20191004
            #print("R = ", R[i])
# omb[4,i] = 0

        for n in range(4):
            omb_mean = np.zeros(5)
            omb_mean[n] = np.mean(omb[n,:])
#print 'shape', omb_mean.shape
            if omb_mean[n]**2 > R[n]*10000: #5 no rejection
                omb[n,:] = 0
                print ('omb=0')
        #omb[0,:] = 0
#print 'omb', omb[3,0]
#print 'obs', obs[3,0], 'ges', ges[3,0]

        #----STEP 3: Data assimilation (analysis step)-----------
        #
        analysis = np.zeros((ndim,nens))
        if counter != 0:
         da = SIRPF(ges,analysis,omb,R,nens,nobs,s2,0)  
         da.sirfilter()
        else: # no observation exists
         analysis = ges


        #----STEP 4: finalizing---------------------------------------
        #
        print("End of cycle ",t)
#print("initial guess mean ", np.mean(ges[:,:],axis=1))
#        print("analysis mean ", np.mean(analysis[:,:],axis=1))
#print("verifying truth ", data[endt+1,:])
        if startt > spinupyr: # in a spin-up period no assimilation
           ges = analysis
#ges[1,:] = ges[1,:]*(1e+5)
        #print('before', ges[2,:])
        ges[0,:] = np.clip(ges[0,:],0,1)  #F
        ges[1,:] = np.clip(ges[1,:],10,1e+50)   #G
        ges[2,:] = np.clip(ges[2,:],1,10000)   #D
        ges[3,:] = np.clip(ges[3,:],0,50)    #H
        ges[4,:] = np.clip(ges[4,:],0,3)    #M
       
        ges[5,:] = np.clip(ges[5,:],0.001,50)
        ges[6,:] = np.clip(ges[6,:],0.0001,10)
        ges[7,:] = np.clip(ges[7,:],0.01,100)
        ges[8,:] = np.clip(ges[8,:],0.001,50)

        ges[9,:] = np.clip(ges[9,:],0.001,0.999)
        ges[10,:] = np.clip(ges[10,:], 0.0001,20)
        ges[11,:] = np.clip(ges[11,:],100,200000)
        ges[12,:] = np.clip(ges[12,:],1,150000)
        ges[13,:] = np.clip(ges[13,:],1000,150000)
        ges[14,:] = np.clip(ges[14,:],0.,0.01)
        ges[15,:] = np.clip(ges[15,:],0.,0.5)


#print('ges1', ges[1,:])
        #for i in range(0,nens):
         # print ('ges[F,G,D,H,M]', ges[0:5,i])

    return(ensemble, ensemblemean)

ensemble, ensemblemean = PFcycle(ges,ncycle)

#print('ensemblemean=', ensemblemean[window*ncycle-1,:])
for index, a in enumerate(['F', 'G', 'D', 'H', 'M', 'XiH', 'AlpH', 'EpsT', 'GamE', 'AlpS', 'RhoE', 'RmdE', 'RmdP', 'PhiP', 'KapT', 'MyuS']):
   print (a, ensemblemean[window*ncycle-1, index])


#----draw figure--------------------------------------------------

##states-----------------------------
fig = plt.figure(figsize=(10,10))
plt.rcParams['font.size']=20
#plt.suptitle(suptitle)

t1=np.arange(1800,1800+window*ncycle,1)
tw=np.arange(0,window*ncycle+1,1)
alltime=window*ncycle
start = 1800
end = 1800+window*ncycle

ax1 = fig.add_subplot(321)
ax1.set_xlim(start,end)
ax1.set_ylim(0,20)
ax1.set_ylabel('W(t)')
#ax1.bar(t1,result[:,0],width=0.4,color='black')
ax1.bar(t1,W,width=1.0,color='black')
#ax1.bar(tw,W,width=0.4,color='black')
plt.title('water level')

ax2 = fig.add_subplot(322)
ax2.set_xlim(start,end)
ax2.set_ylim(0,50)#(0,20)
ax2.set_ylabel('H(t)')
plt.title('levee height')

ax3 = fig.add_subplot(323)
ax3.set_xlim(start,end)
ax3.set_ylim(0,10000)  #600, 2400
ax3.set_ylabel('D(t)')
plt.title('distance from river')

ax4 = fig.add_subplot(324)
ax4.set_xlim(start,end)
ax4.set_ylim(1e+0,1e+12)            # (1e+3,1e+12)
ax4.set_ylabel('G(t)')
plt.yscale('log')
plt.yticks([1e+0,1e+4,1e+8,1e+12])

plt.title('settlement size')

ax5 = fig.add_subplot(325)
ax5.set_xlim(start,end)
ax5.set_ylim(0,1)
ax5.set_ylabel('F(t)')
plt.title('intensity of flooding')

ax6 = fig.add_subplot(326)
ax6.set_xlim(start,end)
ax6.set_ylim(0,2.0)   #0.6
ax6.set_ylabel('M(t)')
plt.title('awareness')

for i in range(0,nens):
    ax2.plot(t1,ensemble[:,3,i],color='darkgray', linewidth=0.6)
    ax3.plot(t1,ensemble[:,2,i],color='darkgray', linewidth=0.6)
    ax4.plot(t1,ensemble[:,1,i],color='darkgray', linewidth=0.6)
    ax5.scatter(t1,ensemble[:,0,i],color='darkgray', s=1)
    ax6.plot(t1,ensemble[:,4,i],color='darkgray', linewidth=0.6)


ax2.plot(t1,ensemblemean[:,3],color='red', label='ensemblemean', linewidth=1.0)
ax3.plot(t1,ensemblemean[:,2],color='red', label='ensemblemean', linewidth=1.0)
#ax3.plot(t1,data[1:,3],color='black', label='true', linewidth=1.0)
ax4.plot(t1,ensemblemean[:,1],color='red', label='ensemblemean', linewidth=1.0)
#ax4.plot(t1,data[1:,2],color='black', label='true', linewidth=1.0)
ax5.scatter(t1,ensemblemean[:,0],color='red', s=3, label='ensemblemean')
#ax5.scatter(t1,data[1:,1],color='black', s=5, label='true')
ax6.plot(t1,ensemblemean[:,4],color='red', label='ensemblemean', linewidth=1.0)
#ax6.plot(t1,data[1:,5],color='black', label='true', linewidth=1.0)

ax2.scatter(t1,obs[:,3],color='green', zorder=nens+2, s=30,label='observation') # obs
ax4.scatter(t1,obs[:,1],color='green', zorder=nens+2, s=30,label='observation') # obs
plt.legend(bbox_to_anchor=(0,-0.15), loc='upper center', ncol=3)
plt.subplots_adjust(wspace=0.41, hspace=0.43, top=0.94,right=0.95,left=0.15)

#plt.savefig('./output/seed10/parameters/states_noobs_'+str(param)+'.png',dpi=200)
#plt.savefig('./output/states_datwindow100_ens5000_movpara.png')
#plt.savefig('./output/seed10/'+str(param)+'/states_obs_HM_'+str(param)+'.png')
plt.savefig('./rome_daG_state.png')
#plt.show()
plt.close()

##parameters-------------

def draw_params(param1, paramnum, true, ymax, ymin, ):   #param=ensemblemean[:,paramnum]
   
    fig = plt.figure(figsize=(8,4.5))
    plt.rcParams['font.size']=20

    #t1=np.arange(0,window*ncycle,1)
    t1=np.arange(1800,1800+window*ncycle,1)
    start = 1800
    end = 1800+window*ncycle
    ax1 = fig.add_subplot(111)
    plt.subplots_adjust(left=0.2,right=0.95,bottom=0.3,top=0.93)
    ax1.set_xlim(start,end)
    ax1.set_ylim(ymin,ymax)
    ax1.set_ylabel(str(param1))
    ax1.set_xlabel('time')

    for i in range(0,nens):
        plt.plot(t1,ensemble[:,paramnum,i],color='darkgray', linewidth=0.6)

#sigma1 = np.average(ensemble[:,paramnum,:], axis=1) + np.std(ensemble[:,paramnum,:], axis=1)
#    sigma2 = np.average(ensemble[:,paramnum,:], axis=1) - np.std(ensemble[:,paramnum,:], axis=1)

#   plt.fill_between(t1, sigma1, sigma2, alpha=0.4, facecolor='dimgrey')

    ax1.plot(t1, ensemblemean[:,paramnum], color='red', label='ensemblemean', linewidth=1.0) 
    if movpara_switch == 1 and param1 == "MyuS":
     tr = true
    else:
     tr = np.ones((window*ncycle))*true
#    ax1.plot(t1, tr, color='black', label='true', linewidth='1.5')
    #sigma1=
    #sigma2=
    #plt.fill_between(t1,sigma1,sigma2, )
# plt.title(suptitle)

    plt.legend(bbox_to_anchor=(0.5,-0.24),loc='upper center',ncol=2)
# plt.savefig('./output/parameters/seed4_0719/'+str(param1)+'_noobs.png',dpi=200)
#    plt.savefig('./output/'+str(param1)+'_obs_all_twindow100_ens5000.png',dpi=200)
    plt.savefig('./rome_daG_'+str(param1)+'.png',dpi=200)
#    plt.savefig('./output/seed10/'+str(param)+'/'+str(param1)+'_obs_HM.png', dpi=200)
#plt.show()
    plt.close('all')
    return(fig)

#graph=draw_params('XiH', 5, 0.5, 6, 0)
graph=draw_params('GamE', 8, 0.5, 5, 0)
#graph=draw_params('AlpH', 6, 0.01, 5, -1)
#graph=draw_params('EpsT', 7, 1.1, 50, 0)
#graph=draw_params('AlpS', 9, 0.5, 0.8, 0)
#graph=draw_params('RhoE', 10, 0.02, 1, 0)
#graph=draw_params('RmdE', 11, 5000, 10000, 0)
#graph=draw_params('RmdP', 12, 12000, 20000, 0)
graph=draw_params('PhiP', 13, 10000, 50000, 1000)
graph=draw_params('KapT', 14, 0.001, 0.002, 0)
if movpara_switch == 1: # Parameter is moving
 MyuStrue = np.zeros((window*ncycle))
 MyuStrue[0:250] = 0.01
 MyuStrue[250:750] = np.linspace(0.01,0.1,500)
 MyuStrue[750:1000] = 0.1
 graph=draw_params('MyuS', 15, MyuStrue, 0.4, 0.0)
else:
 graph=draw_params('MyuS', 15, 0.05, 0.4, 0.0)

## RMSEs -----------------------
def rmsecalc(truth,pred,nens):
 rmse = 0.0
 for i in range(0,nens):
  rmse += np.sqrt(mean_squared_error(truth[:], pred[:,i]))
 rmse = rmse/nens
 return(rmse)

exit()

rmse = rmsecalc(truth[:,1],ensemble[:,1,:],nens)
print('RMSE of G =', rmse)
rmse = rmsecalc(truth[:,2],ensemble[:,2,:],nens)
print('RMSE of D =', rmse)
rmse = rmsecalc(truth[:,3],ensemble[:,3,:],nens)
print('RMSE of H =', rmse)
rmse = rmsecalc(truth[:,4],ensemble[:,4,:],nens)
print('RMSE of M =', rmse)
para = np.zeros((window*ncycle,4))
para[:,0] = GamE1
para[:,1] = PhiP1
para[:,2] = KapT1
para[:,3] = MyuS1
if movpara_switch == 1:
 para[:,3] = MyuStrue[:]
rmse = rmsecalc(para[:,0],ensemble[:,8,:],nens)
print('RMSE of GamE = ', rmse)
rmse = rmsecalc(para[:,1],ensemble[:,13,:],nens)
print('RMSE of PhiP = ', rmse)
rmse = rmsecalc(para[:,2],ensemble[:,14,:],nens)
print('RMSE of KapT = ', rmse)
rmse = rmsecalc(para[:,3],ensemble[:,15,:],nens)
print('RMSE of MyuS = ', rmse)






